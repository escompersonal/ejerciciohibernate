/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebahibernate;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author carlos
 */
@Entity
@Table(name="categorias")
public class Categorias implements Serializable{
    @Id @GeneratedValue (strategy = GenerationType.IDENTITY)
    private int idCategoria;
    private String nombreCategoria;
    private String descripcion;

    
    
    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getNombreCategoria() {
        return nombreCategoria;
    }

    public void setNombreCategoria(String nombreCategoria) {
        this.nombreCategoria = nombreCategoria;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    @Override
    public String toString(){
        return " Id: "+idCategoria+"\n Nombre: "+nombreCategoria+"\n descripcion: "+descripcion;
    }
    
    
}
