/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebahibernate;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;


/**
 *
 * @author carlos
 */
public class CategoriaDAO {
    public void create(Categorias c){
        Session s = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = s.getTransaction();
        try{
            tx.begin();
            s.save(c);
            tx.commit();
        }catch(HibernateException he){
            if(tx != null  && tx.isActive()){
                tx.rollback();
            }
        }
    }
    public void update(Categorias c){
        Session s = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = s.getTransaction();
        try{
            tx.begin();
            s.update(c);
            tx.commit();
        }catch(HibernateException he){
            if(tx != null  && tx.isActive()){
                tx.rollback();
            }
        }
    }
    public void delete(Categorias c){
        Session s = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = s.getTransaction();
        try{
            tx.begin();
            s.delete(c);
            tx.commit();
        }catch(HibernateException he){
            if(tx != null  && tx.isActive()){
                tx.rollback();
            }
        }
    }
    public Categorias read(Categorias c){
        Session s = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = s.getTransaction();
        try{
            tx.begin();
            c = (Categorias) s.get(c.getClass(), c.getIdCategoria()); 
            tx.commit();
        }catch(HibernateException he){
            if(tx != null  && tx.isActive()){
                tx.rollback();
            }
        }
        return c;
    }
    
    public List<Categorias> readAll(){
        List l = null;
        Session s = HibernateUtil.getSessionFactory().getCurrentSession();
        Transaction tx = s.getTransaction();
        
        try{
            tx.begin();
            Query q = s.createQuery("from Categorias");
            l = q.list();
            tx.commit();
            return l;
        }catch(HibernateException he){
            if(tx != null  && tx.isActive()){
                tx.rollback();
            }
            return l;
        }
       
    }
   
}
