/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebahibernate;

import java.util.List;
import java.util.Scanner;

/**
 *
 * @author carlos
 */
public class Manager {

    public static void main(String[] args) {
        int opc;
        Scanner sc = new Scanner(System.in);
        CategoriaDAO cdao = new CategoriaDAO();
        while (true) {
            System.out.println("1) Insertar");
            System.out.println("2) Eliminar");
            System.out.println("3) Modificar");
            System.out.println("4) Seleccionar");
            System.out.println("5) Seleccionar todo");
            System.out.println("Elige una opcion: ");
            opc = sc.nextInt();
            Categorias c = new Categorias();
            switch (opc) {
                case 1:
                    System.out.print("Nombre: ");
                    sc.nextLine();
                    String nomb = sc.nextLine();
                    c.setNombreCategoria(nomb);
                    System.out.print("Descripcion: ");
                    c.setDescripcion(sc.nextLine());
                    cdao.create(c);
                    break;
                case 2:
                    System.out.print("Id a eliminar: ");
                    sc.nextLine();
                    c.setIdCategoria(sc.nextInt());
                    cdao.delete(c);
                    break;
                case 3:
                    Categorias cb;
                    System.out.print("Id modifica: ");
                    sc.nextLine();
                    c.setIdCategoria(sc.nextInt());
                    cb = cdao.read(c);
                    if (cb != null) {
                        System.out.print("Datos actuales \n");
                        System.out.print(cb);
                        System.out.print("\nNombre: ");
                        sc.nextLine();
                        cb.setNombreCategoria(sc.nextLine());
                        System.out.print("Descripcion: ");
                        cb.setDescripcion(sc.nextLine());
                        cdao.update(cb);
                    }
                    break;
                case 4:
                    Categorias cs;
                    System.out.print("Id a buscar: ");
                    sc.nextLine();
                    c.setIdCategoria(sc.nextInt());
                    cs = cdao.read(c);
                    if(cs != null)
                        System.out.println(cs);
                    else
                        System.out.println("Elemento no encontrado");
                    break;
                case 5:
                       List<Categorias> lista;
                       lista = cdao.readAll();
                       if(lista != null){
                           for(int i = 0; i < lista.size();i++){
                               System.out.println("----------------------------------------------");
                               System.out.println(lista.get(i));
                           }
                       }else{
                           System.out.println("tabla vacia");
                       }
                break;
                default:
                break;
            }
            System.out.println("\n----------------------------------------------");

        }

    }
}
